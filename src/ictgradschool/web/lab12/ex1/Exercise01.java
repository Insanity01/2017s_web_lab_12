package ictgradschool.web.lab12.ex1;

import ictgradschool.web.lab12.Keyboard;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.Properties;

public class Exercise01 {
    public static void main(String[] args) {
        /* The following verifies that your JDBC driver is functioning. You may base your solution on this code */
        Properties dbProps = new Properties();
        try (FileInputStream fIn = new FileInputStream("mysql.properties")) {
            dbProps.load(fIn);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Set the database name to your database
        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            System.out.println("Connection successful");

            while (true) {
                System.out.println("Please enter article search: ");
                String searchTerm = "%" + Keyboard.readInput() + "%";

                if (searchTerm.equals("%%")) {
                    continue;
                }
                try (PreparedStatement stmt = conn.prepareStatement("SELECT body FROM simpledao_articles WHERE title LIKE ? ;")) {
                    stmt.setString(1, searchTerm);

                    try (ResultSet results = stmt.executeQuery()) {
                        while (results.next()) {
                            System.out.println(results.getString("body"));
                        }

                    }
                }

            }


        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
