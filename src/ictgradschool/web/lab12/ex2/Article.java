package ictgradschool.web.lab12.ex2;

public class Article {

    private String title;
    private String body;

    public Article(String title, String body) {
        this.title = title;
        this.body = body;
    }

    void setTitle(String title) {
        this.title = title;
    }

    void setBody(String body) {
        this.body = body;
    }

    public String getBody() {
        return body;
    }

    public String getTitle() {
        return title;
    }
}
