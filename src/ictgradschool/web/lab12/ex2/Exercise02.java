package ictgradschool.web.lab12.ex2;

import ictgradschool.web.lab12.Keyboard;
import ictgradschool.web.lab12.ex2.ArticleDao;
import org.jooq.util.mysql.MySQLDatabase;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.List;
import java.util.Properties;

public class Exercise02 {
    public static void main(String[] args) throws IOException, SQLException {
        try (ArticleDao dao = new ArticleDao(new MySQLDatabase())) {

            while (true) {
                System.out.printf("input a partial title of an article: ");
                String s = "%" + Keyboard.readInput() + "%";
                if (s.equals("%%")) {
                    continue;
                }
//                System.out.println("All lecturers: ");
                List<Article> articles = dao.allArticles(s);

                for (Article article : articles) {
                    System.out.println(article.getBody());
                }
            }

        }
    }
}

