package ictgradschool.web.lab12.ex2;

import org.jooq.util.Database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ArticleDao implements AutoCloseable {
    private final Database db;
    private final Connection conn;

    public ArticleDao(Database db) {
        this.db = db;
        this.conn = db.getConnection();

    }

    public List<Article> allArticles(String s) throws SQLException{

        try (PreparedStatement stmt = conn.prepareStatement("SELECT body FROM simpledao_articles WHERE title LIKE ? ;")) {
            stmt.setString(1, s);

            try (ResultSet rs = stmt.executeQuery()) {

                List<Article> articles = new ArrayList<>();

                while (rs.next()) {
                    articles.add(articleFromResultSet(rs));
                }

                return articles;

            }
        }
    }

    private Article articleFromResultSet(ResultSet rs) throws SQLException {

        return new Article(rs.getString(1), rs.getString(2));
    }

    @Override
    public void close() throws SQLException {

    }
}
